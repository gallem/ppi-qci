<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TareaEntregada extends Model
{
    //
    protected $table = "TareaEntregada";
    public $timestamps = false;
}
