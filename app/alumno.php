<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class alumno extends Model
{
    //
    //protected $tablename = "alumno";
    protected $table = 'alumno';
    public $timestamps = false;
    protected $primaryKey = "codigo";
}
