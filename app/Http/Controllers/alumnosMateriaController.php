<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\alumno;
use App\materia;
use App\alumnosMateria;

class alumnosMateriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if ($request->session()->get('tipo') == "A") {
            //$materias = materia::all();
            //$materiasOcupadas = [];
            $materiasOcupadas = alumnosMateria::where("codigoAlumno",$request->session()->get('codigo'))->get();
            $materiasFiltro = [];
            foreach ($materiasOcupadas as $materia) {
                $materiasFiltro[] = $materia->CrnMateria;
            }
            //dump($materiasFiltro);
            $materias = materia::whereNotIn("Crn",$materiasFiltro)->get();

            //dump($materias);
            /*
            $materiasLibres = [];
            foreach ($materias as $materia) {
                foreach ($materiasOcupadas as $materiaOcupada) {
                    if ($materia->Crn == $materiaOcupada->CrnMateria) {
                        //dump($materia);
                        unset($materia);
                        break;
                    }
                }
            }
            */
            //dump($materias);
            return view('alumnosMateria.alumno', ["materias" => $materias]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dump($request);
        $incripcion = new alumnosMateria;
        $incripcion->CrnMateria = $request->CrnMateria;
        $incripcion->CodigoAlumno = $request->CodigoAlumno;
        $incripcionExiste = alumnosMateria::where("CrnMateria",$request->CrnMateria)->where("codigoAlumno",$request->CodigoAlumno)->get();
        if (empty($incripcionExiste[0])) {
            $incripcion->save();
        }
        return redirect("alumnosMateria");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
