<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\alumno;
use App\maestro;
use App\materia;
use App\alumnosMateria;
use Illuminate\Support\Facades\Auth;

class loginController extends Controller
{
    function __construct(){
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $sesion = Session("tipo");
      if(isset($sesion)){
        if ($sesion == "A") {
          return redirect("alumno");
        }else{
          return redirect("maestro");
        }
      }
        return view('login.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //return view("login.create");
        /*$existeAlumno = Alumno::where("codigo","214520198")->get();
        if (isset($existeAlumno[0])) {
            # code...
            dump($existeAlumno[0]);
        }*/
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function validar(Request $request){
        $datos = explode(",",$request->datos);
        $request->session()->put('tipo', $datos[0]);
        $request->session()->put('nombre', $datos[2]);
        $request->session()->put('codigo', $datos[1]);
        if (! isset($datos[5])) {
          $datos[5]='';
        }
        $request->session()->put('imagen', $datos[5]);


        if ($request->ajax()) {
            if ($datos[0] == "A") {
                $existeAlumno = alumno::where("codigo",$datos[1])->get();
                if (!isset($existeAlumno[0])) {
                    $alumno = New alumno;
                    $alumno->Nombre = $datos[2];
                    $alumno->Carrera = $datos[4];
                    $alumno->codigo = $datos[1];
                    $alumno->imagen = $datos[5];
                    $alumno->save();
                }
                $datos["web"] = url("home");
            }else{
                $maestro = new maestro;
                $datos["web"] = url("home");
            }


            return json_encode($datos);
        }
    }

    public function logout(Request $request){
        $request->session()->flush();
        $materias = materia::all();
        foreach ($materias as $materia) {
            $materia->delete();
        }
        $alumnos = alumno::all();
        foreach ($alumnos as $alumno) {
            $alumno->delete();
        }
        $alumnosMateria = alumnosMateria::all();
        foreach ($alumnosMateria as $relacionAlumno) {
            $relacionAlumno->delete();
        }
        return redirect('/');
    }
}
