<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\alumno;
use App\materia;
use App\alumnosMateria;

class homeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        //get json data
        $datosJson = file_get_contents("http://148.202.152.33/horarioMaestro.php");
        $datosOriginales = $datosJson;
        $datosJson = json_decode($datosJson);

        foreach ($datosJson->horario as $horario) {

            $buscarMateria = materia::where("Crn",$horario->crn)->get();
            if (!isset($buscarMateria[0])) {
                $materiaInsert = new materia;
                $materiaInsert->Horario = $horario->horario . " " . $horario->dias;
                $materiaInsert->Crn = $horario->crn;
                $materiaInsert->ClaveMateria = $horario->clave_materia;
                $materiaInsert->Aula = $horario->aula;
                $materiaInsert->nombreMateria = $horario->nombre_materia;
                if ($request->session()->get('tipo') != "A") {
                    //$request->session()->get('codigo') == "214520198"
                    $materia->CodigoMaestro = $request->session()->get('codigo');
                }
                $materiaInsert->save();
            }

            $alumnos = $horario->alumnos = json_decode($horario->alumnos);
            foreach ($alumnos as $alumno) {

                $buscarAlumno = alumno::where("Codigo", $alumno->codigo_alumno)->get();
                if (!isset($buscarAlumno[0])) {
                    $alumnoInsert = new alumno;
                    $alumnoInsert->Nombre = $alumno->nombre_alumno;
                    $alumnoInsert->Carrera = $alumno->carrera;
                    $alumnoInsert->codigo = $alumno->codigo_alumno;
                    $alumnoInsert->save();
                }

                $buscarRelacion = alumnosMateria::where("CodigoAlumno",$alumno->codigo_alumno )->where("CrnMateria",$horario->crn)->get();
                if (!isset($buscarRelacion[0])) {
                    $relacionarAlumno = new alumnosMateria;
                    $relacionarAlumno->CodigoAlumno = $alumno->codigo_alumno;
                    $relacionarAlumno->CrnMateria = $horario->crn;
                    $relacionarAlumno->save();
                }

             }

        }


        /*foreach ($datosJson->horario as $horario) {
            //alta de materias del json
            $buscarMateria = materia::where("Crn",$horario->crn)->get();
            if (!isset($buscarMateria[0])) {
                $materiaInsert = new materia;
                $materiaInsert->Horario = $horario->horario . " " . $horario->dias;
                $materiaInsert->Crn = $horario->crn;
                $materiaInsert->ClaveMateria = $horario->clave_materia;
                $materiaInsert->Aula = $horario->aula;
                $materiaInsert->nombreMateria = $horario->nombre_materia;
                $materiaInsert->save();
            }

            //alta alumnos
            $alumnos = $horario->alumnos = json_decode($horario->alumnos);
            foreach ($alumnos as $alumno) {
                $alumnoInsert = new alumno;
                $alumnoInsert->Nombre = $alumno->nombre_alumno;
                $alumnoInsert->Carrera = $alumno->carrera;
                $alumnoInsert->codigo = $alumno->codigo_alumno;

                $buscarAlumno = alumno::where("Codigo", $alumno->codigo_alumno)->get();
                if (!isset($buscarAlumno[0])) {
                    $alumnoInsert->save();
                }
             }
        }*/





        //$datosJson = json_encode($datosJson);
        //return view('home.index', compact("datosOriginales"));
        dump($datosJson);
        dump($request->session()->all());
        $datos = $request->session()->all();

        if ($datos["tipo"] == "A") {
          return redirect("alumno");
        }else {
          return redirect("maestro");
        }
        
        echo "<a href='".url("alumno")."' title=''>Continuar alumno</a><br>";
        echo "<a href='".url("maestro")."' title=''>Continuar maestro</a>";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
