<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tarea;
use App\TareaEntregada;
use App\TareasMateria;
use App\alumno;
use App\alumnosMateria;

class tareaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('tarea.indexAlumno');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $tarea = new tarea;
        $tarea->numero = $request->numero;
        $tarea->descripcion = $request->descripcion;
        $tarea->CrnMateria = $request->CrnMateria;
        $tarea->save();
        return back();
        //return route("tarea/maestro")->with("Crn",$request->CrnMateria);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /*public function alumno(Request $request){
        $request->CrnMateria;
        $tareasEntregadas = TareaEntregada::where("CodigoAlumno",$request->session()->get('codigo'))->get();
        return view("tarea.alumno");
    }*/

    /*public function maestro(Request $request){
        $tareas = tarea::where("CrnMateria",$request->CrnMateria)->get();
        $alumnosMateria = alumnosMateria::where("CrnMateria", $request->CrnMateria)->get(["CodigoAlumno"]);
        $alumnos = alumno::whereIn("codigo",$alumnosMateria)->get();
        $CrnMateria = $request->CrnMateria;
        //dump($alumnos);
        return view('tarea.maestro',compact("tareas","alumnos","CrnMateria"));
    }*/

    public function maestro2($Crn){
        /*
        */
        $tipo= Session("tipo");
        if (!isset($tipo)) {
          return redirect("/");
        }
        if ($tipo == "A") {
          return redirect("alumno");
        }
        $tareas = tarea::where("CrnMateria",$Crn)->get();
        $alumnosMateria = alumnosMateria::where("CrnMateria", $Crn)->get(["CodigoAlumno"]);
        $alumnos = alumno::whereIn("codigo",$alumnosMateria)->get();
        $CrnMateria = $Crn;
        //dump($alumnos);
        return view('tarea.maestro',compact("tareas","alumnos","CrnMateria"));
    }

    public function entregarTareas(Request $request){
        $tareas = TareaEntregada::where("CrnMateria",$request->CrnMateria)->where("CodigoAlumno",$request->CodigoAlumno)->delete();
        foreach ($request->tareas as $tarea) {
            $tareaNueva = new TareaEntregada;
            $tareaNueva->CrnMateria = $request->CrnMateria;
            $tareaNueva->CodigoAlumno = $request->CodigoAlumno;
            $tareaNueva->IdTarea= $tarea;
            $tareaNueva->save();
        }
        dump($request);
        return back();
    }

    public function alumno($Crn){
        $tipo= Session("tipo");
        if (!isset($tipo)) {
          return redirect("/");
        }
        if ($tipo != "A") {
          return redirect("maestro");
        }
        $tareas = tarea::where("CrnMateria",$Crn)->get();
        $CrnMateria = $Crn;
        return view('tarea.alumno',compact("tareas","CrnMateria"));
    }
}
