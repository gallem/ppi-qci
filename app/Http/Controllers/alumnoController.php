<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\materia;
use App\alumnosMateria;
use App\TareaEntregada;
use App\tarea;


class alumnoController extends Controller
{

    public function __construct(){
        //$this->middleware('guest');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tipo= Session("tipo");
        if (!isset($tipo)) {
          return redirect("/");
        }
        if ($tipo != "A") {
          return redirect("maestro");
        }
        $materiasRegistradas = alumnosMateria::where("CodigoAlumno",$request->session()->get('codigo'))->get();
        $crns = [];
        foreach ($materiasRegistradas as $materiaRegistrada) {
            $crns[] = $materiaRegistrada->CrnMateria;
        }
        $materiasRegistradas = materia::whereIn("Crn",$crns)->get();

        return view('alumno.index',compact("materiasRegistradas"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
