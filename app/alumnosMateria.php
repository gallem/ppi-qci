<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class alumnosMateria extends Model
{
    //
    protected $table = "AlumnosMateria";
    public $timestamps = false;
}
