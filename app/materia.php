<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class materia extends Model
{
    //
    protected $table = "materia";
    public $timestamps = false;
    protected $primaryKey = "Crn";

}
