<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TareasMateria extends Model
{
    //
    protected $table = "TareasMateria";
    public $timestamps = false;
}
