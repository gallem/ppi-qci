@extends('layouts.adminlte')

@section('menu')
	
@endsection

@section('content')
	<div class="col-xs-10">
		<table class="table table-inverse table-striped table-hover">
			<thead>
				<tr>
					<th>Materia</th>
					<th>Horario</th>
					<th>Registrarse</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($materias as $materia)
					<tr>
						<td>{{ $materia->NombreMateria }}</td>
						<td>{{ $materia->Horario }}</td>
						<td>
							<form action="{{ url('alumnosMateria') }}" method="post" accept-charset="utf-8">
								<input type="hidden" name="CrnMateria" value="{{ $materia->Crn }}">
								<input type="hidden" name="CodigoAlumno" value="{{ Session::get('codigo') }}">
								{{ csrf_field() }}
								<button type="subbmit" class="btn btn-primary btn-sm">Incribirse</button>
							</form>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection