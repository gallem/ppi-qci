<!DOCTYPE html>
<html lang="es">
<head>
	<title>Login Calificador CUCEI</title>
	<meta charset="UTF-8">
	<meta name="_token" content="{{ csrf_token() }}"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="{{ asset('Login_v16/images/icons/favicon.ico') }}"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('Login_v16/vendor/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('Login_v16/') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('Login_v16/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('Login_v16/fonts/Linearicons-Free-v1.0.0/icon-font.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('Login_v16/vendor/animate/animate.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('Login_v16/vendor/animsition/css/animsition.min.css') }}">

<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('Login_v16/css/util.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('Login_v16/css/main.css') }}">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('{{ asset('Login_v16/images/rectoria_cucei.jpg') }}');">
			<div class="wrap-login100 p-t-30 p-b-50">
				<span class="login100-form-title p-b-41">
					Inicio de Sesion
				</span>
				<form class="login100-form validate-form p-b-33 p-t-5" action="http://148.202.152.33/2018/datosudeg.php" method="post" name="loginform" id="loginform" >

					<div class="wrap-input100 validate-input" data-validate = "Enter username">
						<input class="input100" type="text" name="codigo" placeholder="User name">
						<span class="focus-input100" data-placeholder="&#xe82a;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password" name="nip" placeholder="Password">
						<span class="focus-input100" data-placeholder="&#xe80f;"></span>
					</div>
					{{ csrf_field() }}
					<div class="container-login100-form-btn m-t-32">
						<button type="submmit" class="btn btn-info" style="width: 90%" id="botonAjax">
							Login
						</button>
					</div>
					<input type="hidden" name="web" class="form-control" id="" value="{{ url('login/validar') }}">
				</form>

			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="{{ asset('jquery/jquery.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('Login_v16/vendor/animsition/js/animsition.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('Login_v16/vendor/bootstrap/js/popper.js') }}"></script>
	<script src="{{ asset('Login_v16//vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('Login_v16/js/main.js') }}"></script>
	<script src="{{ asset('profe.js') }}" type="text/javascript" charset="utf-8"></script>

<div id="respuestaajax">
</div>
</body>
</html>