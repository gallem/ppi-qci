@extends('layouts.adminlte')
@section('menu')
@endsection

@section('breadcrumb')
	<li><a href="{{ url("maestro") }}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li class="active">Materias</li>
@endsection

@section('content')
	<table class="table table-hover">
		<thead>
			<tr>
				<th>Datos</th>
				<th>Horario</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($materias as $materia)
				<tr>
					<td>{{ $materia->NombreMateria }}</td>
					<td>{{ $materia->Horario }}</td>
					<td>
						<!--form action="{{ url('tarea/maestro') }}" method="post" accept-charset="utf-8">
							{{ csrf_field() }}
							<input type="hidden" name="CrnMateria" value="{{ $materia->Crn }}">
							<button type="" class="btn btn-sm btn-primary">Taeas</button>
						</form-->
						<a href="{{ url('tarea/maestro') }}/{{ $materia->Crn }}" title="" class="btn btn-sm btn-primary">Tarea</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection
