@extends('layouts.adminlte')

@section('breadcrumb')
	<li><a href="{{ url("alumno") }}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li class="active">Home</li>
@endsection

@section('menu')
	<li><a href="{{ url('alumnosMateria') }}"><i class="fa fa-link"></i> <span>Incribirse a curso</span></a></li>
@endsection

@section('content')
	<div class="col-xs-12">
		<h3>Materias Registradas</h3>
		<table class="table table-hover table-condensed">
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Horario</th>
					<th>Accion</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($materiasRegistradas as $materiaRegistrada)
				<tr>
					<td>{{ $materiaRegistrada->NombreMateria }}</td>
					<td>{{ $materiaRegistrada->Horario }}</td>
					<td>
						<!--form action="{{ url('tarea/alumno') }}" method="post" accept-charset="utf-8">
							{{ csrf_field() }}
							<input type="hidden" name="CrnMateria" value="{{ $materiaRegistrada->Crn }}">
							<button type="" class="btn btn-sm btn-primary">Taeas</button>
						</form-->
						<a href="{{ url('tarea/alumno') }}/{{ $materiaRegistrada->Crn }}" title="">Tarea</a>
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
@endsection
