@extends('layouts.adminlte')
@section('menu')

@endsection

@section('breadcrumb')
	<li><a href="{{ url("maestro") }}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="{{ url("maestro") }}"><i class="fa fa-key"></i> Materias</a></li>
	<li><a href="{{ url("tarea/maestro") }}/{{ $CrnMateria }}"><i class="fas fa-adjust"></i> Tareas Materia</a></li>

@endsection

@section('content')
	@php
		use App\TareaEntregada;
	@endphp
	<div class="row">
		<div class="col-xs-12">
			<h2>Crear una Nueva tarea</h2>
			<form action="{{ url('tarea') }}" method="post" accept-charset="utf-8" class="form">
				<div class="form-group">
					<label class="">Numero De Tarea</label>
					<input type="number" name="numero" class="form-control">
				</div>
				<div class="form-group">
					<label class="">Descripcion de la tarea</label>
					<textarea name="descripcion" class="form-control"></textarea>
				</div>
				<input type="hidden" name="CrnMateria" value="{{ $CrnMateria }}">
				{{ csrf_field() }}
				<button type="submmit">Guardar</button>
			</form>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<h2>Eliminar Tarea</h2>

		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			@php
				$cont = 0;
			@endphp
			@foreach ($alumnos as $alumno)
				<div class="panel-group">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
							<a data-toggle="collapse" href="#collapse{{ $cont }}">{{ $alumno->Nombre }}</a>
							</h4>
							</div>
							<div id="collapse{{ $cont }}" class="panel-collapse collapse">
							<div class="panel-body">
								<form action="{{ url('tarea/entregartareas') }}" method="post" accept-charset="utf-8">
								@foreach ($tareas as $tarea)
									@php
										$entregada = TareaEntregada::where("CrnMateria",$CrnMateria)->where("IdTarea",$tarea->id)->where("CodigoAlumno",$alumno->codigo)->get();
									@endphp
										<div class="form-group">
											<input type="checkbox" name="tareas[]" value="{{ $tarea->id }}"
											@if (isset($entregada[0]))
												checked="true"
											@endif
											>
											{{ $tarea->descripcion }}
										</div>
								@endforeach
								<input type="hidden" name="CodigoAlumno" value="{{ $alumno->codigo }}">
								<input type="hidden" name="CrnMateria" value="{{ $CrnMateria }}">
								{{ csrf_field() }}
								<button type="submmit">Enviar</button>
								</form>
							</div>
						</div>
					</div>
				</div>
				@php
					$cont++;
				@endphp
			@endforeach
		</div>
	</div>
@endsection
