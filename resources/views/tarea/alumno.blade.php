@extends('layouts.adminlte')
@section('breadcrumb')
	<li><a href="{{ url("alumno") }}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li class="active">Tarea</li>
@endsection

@section('menu')

@endsection

@section('content')
<div class="row">
	<div class="col-xs-12">
		<h2>Tareas de la materia</h2>
		@php
			use App\TareaEntregada;
			$entregada = 0;
			$noentregada = 0;
		@endphp
		@foreach ($tareas as $tarea)
			@php
				$existe = TareaEntregada::where("CrnMateria",$CrnMateria)->where("CodigoAlumno",Session::get("codigo"))->where("IdTarea",$tarea->id)->get();
			@endphp
			<div class="col-xs-12">
				<input type="checkbox" name="" value=""
				@php
					if (isset($existe[0])) {
						echo 'checked="true"';
						$entregada ++;
					}else{
						$noentregada++;
					}
				@endphp
				>{{ $tarea->descripcion }}
			</div>
		@endforeach
	</div>
</div>


	<!--Load the AJAX API-->
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script type="text/javascript">

	  // Load the Visualization API and the corechart package.
	  google.charts.load('current', {'packages':['corechart']});

	  // Set a callback to run when the Google Visualization API is loaded.
	  google.charts.setOnLoadCallback(drawChart);

	  // Callback that creates and populates a data table,
	  // instantiates the pie chart, passes in the data and
	  // draws it.
	  function drawChart() {

	    // Create the data table.
	    var data = new google.visualization.DataTable();
	    data.addColumn('string', 'Topping');
	    data.addColumn('number', 'Slices');
	    data.addRows([
	      ['Entregadas', {{ $entregada }}],
	      ['No Entregadas', {{ $noentregada }}],
	    ]);

	    // Set chart options
	    var options = {'title':'Tareas Entregadas',
	                   'width': "100%",
	                   'height': "100%",
	                    is3D: true,
	};

	    // Instantiate and draw our chart, passing in some options.
	    var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
	    chart.draw(data, options);
	  }
	</script>

	<!--Div that will hold the pie chart-->
	<div class="row">
		<div id="chart_div"></div>

	</div>
@endsection
