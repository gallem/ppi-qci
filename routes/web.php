<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "loginController@index")->name("login");

Route::resource('login', 'loginController');
Route::post('login/validar', 'loginController@validar');
Route::post('login/logout', 'loginController@logout');

Route::resource('home', 'homeController');

Route::resource('alumno', 'alumnoController');

Route::resource('maestro', 'maestroController');
//Route::resource('maestro', 'maestroController')->middleware("auth");

Route::resource('alumnosMateria', 'alumnosMateriaController');

Route::resource('tarea', 'tareaController');
Route::Post('tarea/alumno', "tareaController@alumno");
Route::post('tarea/maestro', "tareaController@maestro");
Route::get('tarea/maestro/{id}', "tareaController@maestro2");
Route::get('tarea/alumno/{id}', "tareaController@alumno");
Route::post('tarea/entregartareas',"tareaController@entregarTareas");

/*Route::middleware(["auth"])->group(function(){
	Route::resource('alumno', 'alumnoController');
});*/
